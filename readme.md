[![Node Version](https://img.shields.io/badge/Node_Version-%3E%3D_18.0.0-blue)]()
[![NPM Version](https://img.shields.io/badge/NPM_Version-%3E%3D_9.0.0-blue)]()<br>
[![Modern Normalize](https://img.shields.io/badge/Modern_Normalize_CSS-2.0.0-blue)](https://github.com/sindresorhus/modern-normalize)
[![BEM](https://img.shields.io/badge/CSS_Convention-BEM-blue)](https://getbem.com/)<br>
[![Semantic Versioning](https://img.shields.io/badge/Semantic_Versioning-2.0.0-green)](https://semver.org/)
[![Conventional Commits](https://img.shields.io/badge/Conventional_Commits-1.0.0-FE5196)](https://conventionalcommits.org)<br>
[![Commitizen](https://img.shields.io/badge/Commitizen-conventional_commits-FE5196)](https://github.com/commitizen-tools/commitizen)
[![Semantic Release](https://img.shields.io/badge/Semantic_Release-conventional_commits-FE5196)](https://github.com/semantic-release/semantic-release)

<br>

# ISOA CSS Library

This CSS library is intended for personnal use along with all other ISOA projects.<br>

It _can_ be used directly in project, and probably customized.
Though, I intended it to be used by a custom CSS Library / Component Framework.

It's also intended to be using [Material Design Icons](https://pictogrammers.com/library/mdi/). Some selectors directly refers to MDI specific class (`.mdi`).

<br>

# Documentation

Detailed documentation can be found [here](docs/index.md)

<br>

# Examples

Examples of different parts of the CSS Library can be found [here](examples/)
