# Colors

| Color                                                               | Hex     | Name             | Light Role | Dark Role  |
| ------------------------------------------------------------------- | ------- | ---------------- | ---------- | ---------- |
| <div style="width:24px;height:24px;background-color:#000000"></div> | #000000 | Black            | None       | None       |
| <div style="width:24px;height:24px;background-color:#FFFFFF"></div> | #FFFFFF | White            | None       | None       |
|                                                                     |         |                  |            |            |
| <div style="width:24px;height:24px;background-color:#0A1931"></div> | #0A1931 | Blue Oxford      | Text       | Background |
| <div style="width:24px;height:24px;background-color:#EFEFEF"></div> | #EFEFEF | White Anti Flash | Background | Text       |
|                                                                     |         |                  |            |            |
| <div style="width:24px;height:24px;background-color:#FFC947"></div> | #FFC947 | Yellow Sunglow   | Secondary  | Primary    |
| <div style="width:24px;height:24px;background-color:#185ADB"></div> | #185ADB | Blue Tang        | Primary    | Secondary  |
| <div style="width:24px;height:24px;background-color:#7952B3"></div> | #7952B3 | Purple Royal     | Tertiary   | Tertiary   |
|                                                                     |         |                  |            |            |
| <div style="width:24px;height:24px;background-color:#09814A"></div> | #09814A | Green Sea        | Success    | Success    |
| <div style="width:24px;height:24px;background-color:#CF5C36"></div> | #CF5C36 | Orange Flame     | Warning    | Warning    |
| <div style="width:24px;height:24px;background-color:#931F1D"></div> | #931F1D | Red Carmine      | Danger     | Danger     |

All colors are "saved" and used with their fullname, lower and snake-case.<br>
`White Anti Flash` becomes `white-anti-flash`

# Dark mode

Classes can be set to change colors dynamicaly depending on the `prefers-color-scheme` used
They are created following this schema:

- Color scheme to apply the color to (`light:` or `dark:`)
- CSS target (`text` or `background`)
- Color name (`--yellow-sunglow` or any previously listed)

#### Full example:

````html
<div
  class="light:background--white-anti-flash light:text--blue-tang dark:background--blue-oxford dark:text--yellow-sunglow"
>
  TEST
</div>
```
````
