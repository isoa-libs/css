# Global

### Screen sizes

Only one screen size is used, **`tablet`**, has a breakpoint **superior than 640px**

### `.root`

Set the tag dimensions with `width: 100vw` and `height: 100vh` .
