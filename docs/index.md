# Table of Contents

- [Table of Contents](#table-of-contents)
- [GLOBAL](#global)
  - [Themes](#themes)
  - [Base](#base)
- [COMPONENTS](#components)
  - [Layout](#layout)
  - [Collapsible](#collapsible)

<br>
<br>

# GLOBAL

Helpers / Utilities

## [Themes](/docs/global/themes.md)

- [Colors](/docs/global/themes.md#colors)
- [Dark mode (prefers-color-scheme)](/docs/global/themes.md#dark-mode)

<br>

## [Base](/docs/global/base.md)

- [Screens Size](/docs/global/base.md#screen-sizes)
- [Global](/docs/global/base.md#global)

# COMPONENTS

## [Layout](/docs/components/layout.md)

- [Column](/docs/components/layout.md#column)
- [Row](/docs/components/layout.md#row)
- [Sidebar Control](/docs/components/layout.md#sidebar-control)
- [Sidebar](/docs/components/layout.md#sidebar)
- [Navbar](/docs/components/layout.md#navbar)
- [Content](/docs/components/layout.md#content)
- [Footer](/docs/components/layout.md#footer)

## [Collapsible](/docs/components/collapsible.md)

- [Collapsibles](/docs/components/collapsible.md#collapsibles)
- [Collapsible](/docs/components/collapsible.md#collapsible)
- [Collapsible Content](/docs/components/collapsible.md#collapsible-content)
