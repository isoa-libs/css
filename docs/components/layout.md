# Table of Contents

- [Table of Contents](#table-of-contents)
- [Column](#column)
    - [Example](#example)
- [Row](#row)
    - [Example](#example-1)
- [Sidebar Control](#sidebar-control)
    - [Input](#input)
    - [Label](#label)
    - [Example](#example-2)
- [Sidebar](#sidebar)
    - [Head](#head)
    - [Body](#body)
    - [Foot](#foot)
    - [Elem](#elem)
    - [Example](#example-3)
- [Navbar](#navbar)
    - [Elem](#elem-1)
    - [Example](#example-4)
- [Content](#content)
    - [Example](#example-5)
- [Footer](#footer)
    - [Logo](#logo)
    - [Elem](#elem-2)
    - [Example](#example-6)

<br>
<br>

# Column

**`.layout-column`** is a simple `flex column` taking 100% of the height and width available.

### Example

```html
<div class="layout-column">
  <div class="layout-navbar"></div>
  <div class="layout-content"></div>
  <div class="layout-footer"></div>
</div>
```

<br>
<br>

# Row

**`.layout-row`** is similar to `.layout-column`, but in a `row direction`.

### Example

```html
<div class="layout-row">
  <div class="layout-content"></div>
</div>
```

<br>
<br>

# Sidebar Control

Pure HTML elements & CSS selectors to display / hide the sidebar and switch the icon.

> **SHOULD** be used if you intend to use a _display/hide_ `sidebar`.

### Input

- `<input type=checkbox id=sidebar-control />`. <br>
  Doing so, the `.sidebar-control` will hide the input and act as a checkbox.

### Label

- `<label for="sidebar-control" class="sidebar-control__label">icon</label>`. <br>
  Used to display / hide the icons and control the status of the input checkbox.<br>
  Use any of these on the label to style the icon and apply the animation.
  - `.sidebar-control__icon` to set style to the icon (`font-size`).
  - `.sidebar-control__icon--open` or `.sidebar-control__icon--close` to specify which icon should be displayed in open and close state.

### Example

```html
<div class="sidebar-control">
  <input type="checkbox" id="sidebar-control" />
  <div class="layout-row">
    <div class="layout-sidebar">
      <label for="sidebar-control" class="sidebar-control__label">
        <span class="sidebar-control__icon sidebar-control__icon--open"></span>
        <span class="sidebar-control__icon sidebar-control__icon--close"></span>
      </label>
    </div>
    <div class="layout-column">
      <div class="layout-navbar"></div>
      <div class="layout-content"></div>
    </div>
  </div>
</div>
```

<br>
<br>

# Sidebar

**`.layout-sidebar`** will use `flex` properties to `transition` its display.<br>
Colors are set automatically depending on the `prefers-color-scheme`.<br>

Use `.sidebar` as direct child. Create a grid inside it, with a header, list of apps and footer.<br>

### Head

The header, `.sidebar__head`, has a fixed height (same as navbar, `2rem + 1rem of padding`).

### Body

The body, `.sidebar__body`, will take as much space as possible, with a scroll if necessary. Simple column with `row-gap`.

### Foot

The footer, `.sidebar__foot`, has a fixed height (`4rem + 1rem of padding`).

### Elem

The element, `.sidebar__elem`, is intended to be used as a child of any of the above.<br> It will center everything inside and apply a gap to every child.<br>

> Also make any `.mdi` icon slightly bigger.

### Example

```html
<div class="layout-sidebar">
  <div class="sidebar">
    <div class="sidebar__head">
      <label for="sidebar-control" class="sidebar-control__label">
        <span class="sidebar-control__icon sidebar-control__icon--open"></span>
        <span class="sidebar-control__icon sidebar-control__icon--close"></span>
      </label>
    </div>
    <div class="sidebar__body">
      <a class="sidebar__elem" href="#">
        <span class="mdi mdi-lock"></span>
        <span>Vault</span>
      </a>
    </div>
    <div class="sidebar__foot">
      <div class="sidebar__elem">
        <p>Something</p>
      </div>
    </div>
  </div>
</div>
```

<br>
<br>

# Navbar

**`.layout-navbar`** set the height at `3rem`.

Colors are set automatically depending on the `prefers-color-scheme`.<br>

Use `.navbar` as direct child, it's a `row` with space between every child along with some minor style.

### Elem

The element, `.navbar__element`, center everything inside and apply a gap to every child.

> Also make any `.mdi` icon slightly bigger.

### Example

```html
<div class="layout-column">
  <div class="layout-navbar">
    <div class="navbar">
      <div class="navbar__elem">Elem</div>
      <div class="navbar__elem">Elem</div>
    </div>
  </div>
  <div class="layout-content"></div>
  <div class="footer"></div>
</div>
```

<br>
<br>

# Content

**`.layout-content`** take 100% of width and height available.<br>

Colors are set automatically depending on the `prefers-color-scheme`.<br>

Use `.content` as direct child. Only add some padding for now.

### Example

```html
<div class="layout-content">
  <div class="content"></div>
  <div class="footer"></div>
</div>
```

<br>
<br>

# Footer

**`.footer`** create a grid with 1 row, 4 columns and add some padding.

Colors are set automatically depending on the `prefers-color-scheme`.<br>

### Logo

The logo, `.footer__logo`, justify itself in the center. Set logo img height to `3rem` and auto adjust width.

### Elem

The element, `.footer__elem`, justify itself at the start

### Example

```html
<div class="footer">
  <div class="footer__logo"><img src="" /></div>
  <div class="footer__elem">
    <p>Second Column</p>
    <p>Some Text</p>
  </div>
  <div class="footer__elem">
    <p>Third Column</p>
    <p>Some Text</p>
  </div>
  <div class="footer__elem">
    <p>Forth Column</p>
    <p>Some Text</p>
  </div>
</div>
```
