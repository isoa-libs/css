# Table of Contents

- [Table of Contents](#table-of-contents)
- [Collapsibles](#collapsibles)
    - [Example](#example)
- [Collapsible](#collapsible)
    - [Input](#input)
    - [Label](#label)
    - [Content](#content)
    - [Example](#example-1)
- [Collapsible Content](#collapsible-content)
    - [Divider](#divider)
    - [Text](#text)
    - [Example](#example-2)

# Collapsibles

Simple flex column, spacing out child (using `row-gap: 1.5rem`).

### Example

```html
<div class="collapsibles">
  <div class="collapsible collapsible--border"></div>
  <div class="collapsible collapsible--border"></div>
  <div class="collapsible collapsible--border"></div>
</div>
```

<br>
<br>

# Collapsible

Pure CSS collapsible with animation. Ideal for FAQ.

Colors are set automatically depending on the `prefers-color-scheme`.<br>

<br>

Has the following modifiers:

- `--border` : Add a solid border of 1px
- `--shadow` : Add a shadow around the collapsible

### Input

Parent `.collapsible` will hide the direct child `input`, no matter the `id` or `class`.

- `<input type=checkbox id=collapsible-one />`. Will act as a checkbox<br>

### Label

Used to display / hide the _content_ and control the status of the input checkbox.

- `<label for="collapsible-one" class="collapsible__label"></label>`. <br>

### Content

`.collapsible__content` will be displayed when the checkbox is checked, via the use of `grid`.<br>
Combine with `--transition` to add a display transition.

> Should have only one child, `.collapsible-content` .

### Example

```html
<div class="collapsible collapsible--border collapsible--shadow">
  <input id="collapsible-one" type="checkbox" />
  <label for="collapsible-one" class="collapsible__label"> Title </label>
  <div class="collapsible__content collapsible__content--transition">
    <div class="collapsible-content"></div>
  </div>
</div>
```

<br>
<br>

# Collapsible Content

`.collapsible-content` is neccessary to be hidden by `.collapsible__content` with overflow.

### Divider

Custom `<hr>`, `.collapsible-content__divider`, use theme color and have no margin.

Colors are set automatically depending on the `prefers-color-scheme`.<br>

### Text

`.collapsible-content__text` simply set some margin.

### Example

```html
<div class="collapsible__content">
  <div class="collapsible-content">
    <hr class="collapsible-content__divider" />
    <p class="collapsible-content__text"></p>
  </div>
</div>
```
